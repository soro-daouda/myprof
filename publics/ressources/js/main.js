
$(".number-1").mouseenter(function () {
    $(".content-1").css('boxShadow', '0px 0px 8px 0px #e2dfdf');
    $(".content-1").css('transform', 'scale(1.1)');
    $(".content-1").css('transition', 'all .3s');
});
$(".number-1").mouseleave(function () {
    $(".content-1").css('boxShadow', 'initial');
    $(".content-1").css('transform', 'initial');
    $(".content-1").css('transition', 'all .3s');
});
//---------
$(".number-2").mouseenter(function () {
    $(".content-2").css('boxShadow', '0px 0px 8px 0px #e2dfdf');
    $(".content-2").css('transform', 'scale(1.1)');
    $(".content-2").css('transition', 'all .3s');
});
$(".number-2").mouseleave(function () {
    $(".content-2").css('boxShadow', 'initial');
    $(".content-2").css('transform', 'initial');
    $(".content-2").css('transition', 'all .3s');
});
//---------
$(".number-3").mouseenter(function () {
    $(".content-3").css('boxShadow', '0px 0px 8px 0px #e2dfdf');
    $(".content-3").css('transform', 'scale(1.1)');
    $(".content-3").css('transition', 'all .3s');
});
$(".number-3").mouseleave(function () {
    $(".content-3").css('boxShadow', 'initial');
    $(".content-3").css('transform', 'initial');
    $(".content-3").css('transition', 'all .3s');
});

// responsive nav
$('.menu-bar').click(function () {
    $(".link").toggle("slide",);
});

// responsive nav
$('.img-profil').click(function () {
    $(".sous-menu").toggle("slide");
});

// btn like
$(".btn-likeReponse").click(function () {
    $(".btn-likeReponse").toggleClass("likereponse");
});


// dropzone
$(function () {

    $('#dropzone').on('dragover', function () {
        $(this).addClass('hover');
    });

    $('#dropzone').on('dragleave', function () {
        $(this).removeClass('hover');
    });

    $('#dropzone input').on('change', function (e) {
        var file = this.files[0];

        $('#dropzone').removeClass('hover');

        if (this.accept && $.inArray(file.type, this.accept.split(/, ?/)) == -1) {
            return alert('File type not allowed.');
        }

        $('#dropzone').addClass('dropped');
        $('#dropzone img').remove();

        if ((/^image\/(gif|png|jpeg)$/i).test(file.type)) {
            var reader = new FileReader(file);

            reader.readAsDataURL(file);

            reader.onload = function (e) {
                var data = e.target.result,
                    $img = $('<img />').attr('src', data).fadeIn();

                $('#dropzone div').html($img);
            };
        } else {
            var ext = file.name.split('.').pop();

            $('#dropzone div').html(ext);
        }
    });
});


// owl carousel
$('.owl-carousel').owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    autoplay: false,
    dots: false,
    navText: [
        '<div class="next-postProf next-postProf-1"><img src="../ressources/images/flaticon/next-info.svg" alt="suivant" class="img-fluid"></div>',
        // '<i class="fa fa-angle-right" aria-hidden="true"></i>'
        '<div class="next-postProf" aria-hidden="true"><img src="../ressources/images/flaticon/next-info.svg" alt="suivant" class="img-fluid"></div>'
    ],
    responsive: {
        0: {
            items: 1,
            nav: true
        },
        600: {
            items: 2,
            nav: false
        },
        1000: {
            items: 2,
            nav: true,
            loop: false
        }
    }
})


// select type forum
$('select.dropdown').each(function () {

    var dropdown = $('<div />').addClass('dropdown selectDropdown');

    $(this).wrap(dropdown);

    var label = $('<span />').text($(this).attr('placeholder')).insertAfter($(this));
    var list = $('<ul />');

    $(this).find('option').each(function () {
        list.append($('<li />').append($('<a />').text($(this).text())));
    });

    list.insertAfter($(this));

    if ($(this).find('option:selected').length) {
        label.text($(this).find('option:selected').text());
        list.find('li:contains(' + $(this).find('option:selected').text() + ')').addClass('active');
        $(this).parent().addClass('filled');
    }

});

$(document).on('click touch', '.selectDropdown ul li a', function (e) {
    e.preventDefault();
    var dropdown = $(this).parent().parent().parent();
    var active = $(this).parent().hasClass('active');
    var label = active ? dropdown.find('select').attr('placeholder') : $(this).text();

    dropdown.find('option').prop('selected', false);
    dropdown.find('ul li').removeClass('active');

    dropdown.toggleClass('filled', !active);
    dropdown.children('span').text(label);

    if (!active) {
        dropdown.find('option:contains(' + $(this).text() + ')').prop('selected', true);
        $(this).parent().addClass('active');
    }

    dropdown.removeClass('open');
});

$('.dropdown > span').on('click touch', function (e) {
    var self = $(this).parent();
    self.toggleClass('open');
});

$(document).on('click touch', function (e) {
    var dropdown = $('.dropdown');
    if (dropdown !== e.target && !dropdown.has(e.target).length) {
        dropdown.removeClass('open');
    }
});


// form progress
// var currentTab = 0; // Current tab is set to be the first tab (0)
// showTab(currentTab); // Display the crurrent tab

// function showTab(n) {
//     // This function will display the specified tab of the form...
//     var x = document.getElementsByClassName("tab");
//     x[n].style.display = "block";
//     //... and fix the Previous/Next buttons:
//     if (n == 0) {
//         document.getElementById("prevBtn").style.display = "none";
//     } else {
//         document.getElementById("prevBtn").style.display = "inline";
//     }
//     if (n == (x.length - 1)) {
//         document.getElementById("nextBtn").innerHTML = "Aller à mon tableau de bord";
//     } else {
//         document.getElementById("nextBtn").innerHTML = "Suivant";
//     }
//     //... and run a function that will display the correct step indicator:
//     fixStepIndicator(n)
// }

// function nextPrev(n) {
//     // This function will figure out which tab to display
//     var x = document.getElementsByClassName("tab");
//     // Exit the function if any field in the current tab is invalid:
//     if (n == 1 && !validateForm()) return false;
//     // Hide the current tab:
//     x[currentTab].style.display = "none";
//     // Increase or decrease the current tab by 1:
//     currentTab = currentTab + n;
//     // if you have reached the end of the form...
//     if (currentTab >= x.length) {
//         // ... the form gets submitted:
//         document.getElementById("regForm").submit();
//         return false;
//     }
//     // Otherwise, display the correct tab:
//     showTab(currentTab);
// }

// function validateForm() {
//     // This function deals with validation of the form fields
//     var x, y, i, valid = true;
//     x = document.getElementsByClassName("tab");
//     y = x[currentTab].getElementsByTagName("input");
//     // A loop that checks every input field in the current tab:
//     for (i = 0; i < y.length; i++) {
//         // If a field is empty...
//         if (y[i].value == "") {
//             // add an "invalid" class to the field:
//             y[i].className += " invalid";
//             // and set the current valid status to false
//             valid = false;
//         }
//     }
//     // If the valid status is true, mark the step as finished and valid:
//     if (valid) {
//         document.getElementsByClassName("step")[currentTab].className += " finish";
//     }
//     return valid; // return the valid status
// }

// function fixStepIndicator(n) {
//     // This function removes the "active" class of all steps...
//     var i, x = document.getElementsByClassName("step");
//     for (i = 0; i < x.length; i++) {
//         x[i].className = x[i].className.replace(" active", "");
//     }
//     //... and adds the "active" class on the current step:
//     x[n].className += "active";
// }


// form post annonce

// let afficheInput = true;
// $(".inputChecked").on('click', function () {
//     if(afficheInput){
//         if ($(this).children('.input-check').is(':checked')) {
//             $(this).children('.input-check').removeAttr('checked');
//             $(this).css('background-color', 'rgba(134, 134, 134, .1)');
//             $(this).css('color', 'black');
//             this.removeChild(this.children[2]);
//         } else {
//             this.children[0].setAttribute('checked', '');
//             $(this).css('background-color', 'rgb(72 193 126)');
//             $(this).css('color', 'white');
//             $(this).append('<input type="text" value="" class="addInput" name="" id="" placeholder="" onmouseenter="return setFalse();" onmouseleave="return setTrue();" onblur="return setTrue();"/>')
//         }
//     }
// });
// $('.input-check').each(function(){
//     $(this).on('click',function(){
//         if($(this).is(':checked')){
//             console.log('oui');
//             $(this).parent('.inputChecked').css('background-color', 'rgba(134, 134, 134, .1)');
//             $(this).parent('.inputChecked').css('color', 'white');
//             this.parentElement.innerHMTL += '<input type="text" value="" class="addInput" name="" id="" placeholder="" onmouseenter="return setFalse();" onmouseleave="return setTrue();" onblur="return setTrue();"/>';
//         }else{
//             console.log('non');
//             $(this).parent('.inputChecked').css('background-color', 'rgb(72 193 126)');
//             $(this).parent('.inputChecked').css('color', 'black');
//             this.parentElement.removeChild(this.nextElementSibling); 
//         }
//     });
// })

// const setFalse = () => {
//     afficheInput = false;
// }

// const setTrue = () => {
//     afficheInput = true;
// }

// --------------

$(".checked").on('click', function(){
    if($(this).children('input').is(':checked')){
        $(this).children('input').removeAttr('checked');
        $(this).css('background-color', 'rgba(134, 134, 134, .1)');
        $(this).css('color', 'black');
    }else{
        this.children[0].setAttribute('checked','');
        $(this).css('background-color', 'rgb(72 193 126)');
        $(this).css('color', 'white');
    }
})

// --------------

$(".consigne-textarea").on('focus', function(){
    $(this).next().css('display', 'block');
})

$(".consigne-textarea").on('blur', function () {
    $(this).next().css('display', 'none');
})

// --------------

$(".checked-consigne").on('click', function () {
    if ($(this).children('input').is(':checked')) {
        $(this).children('input').removeAttr('checked');
        $(this).css('background-color', 'rgba(134, 134, 134, .1)');
        $(this).css('color', 'black');

        $(".titre-consigne").css('display','none')
    } else {
        this.children[0].setAttribute('checked', '');
        $(this).css('background-color', 'rgb(72 193 126)');
        $(this).css('color', 'white');

        $(".titre-consigne").css('display', 'block')
    }
})

// --------------

$(".check-langue").on('click', function(){
    $(this).toggleClass("bg-langue")
})

// -------------

// loader -----------
let time = setInterval(() => {
    if (document.readyState == "complete") {
        // console.log("fini")
        $(".loaderI").hide();
        clearInterval(time);
    }
}, 1000)